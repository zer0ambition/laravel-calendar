<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UserAlreadyHasEventsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Gate;

class UpdateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $date = $this->request->all();
        $date = $date['date'];
        $rules = [
            'date' => 'sometimes|required|date|after:today',
            'description' => 'sometimes|required|string',
            'name' => 'sometimes|required|string',
        ];

        if (Gate::allows('add-other-users-to-event')) {
            $rules = array_merge($rules,
                [
                    "users"    => "sometimes|required|array",
                    "users.*"  =>
                        ["sometimes", "required", "numeric", "distinct", "exists:users,id", new UserAlreadyHasEventsRule($date)],
                ]
            );
        }

        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(new JsonResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422));
    }
}
