<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Response\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\User;

class UserAlreadyHasEventsRule implements Rule
{
    protected $date;
    public $user;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($date)
    {
        $this->date = $date;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->user = $value;
        $date = new \DateTime($this->date);
        $date = $date->format('Y-m-d');
        $user = User::find($value);
        $count = $user->events()->whereDate('date', '=', "$date")->count();
        if ($count >= 3) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        throw new HttpResponseException(new JsonResponse(['message' => "Specified user $this->user already has 3 events at this date."], 400));
    }
}
