<?php

namespace App\Models;

use App\Rules\UserAlreadyHasEventsRule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'name',
        'description',
        'user_create',
        'users_attach'
    ];

    /**
     * Sortable fields in model
     *
     * @var array
     */
    public const SORT_FIELDS = [
        'date',
        'name',
        'id',
        'description'
    ];

    /**
     * Relation users table
     *
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'event_user', 'event_id', 'user_id')->using('App\Models\EventUser');
    }

    /**
     * Relation user table
     *
     * @return HasOne
     */
    public function userCreate()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_create');
    }
}
