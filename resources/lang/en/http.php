<?php

return [
    'delete' => 'Successfully deleted :item',
    'patch' => 'Successfully patched :item',
    'create' => 'Successfully created :item',
    'resource_not_found' => 'Resource :resource not found',
    'route_not_found' => 'Route not found',
    'wrong_data' => 'Wrong data in request',
    'too_many' => 'Too many requests',
    'forbidden' => 'Forbidden',
    'file_not_valid' => 'File :name is not valid.',
    'avatar_not_valid' => 'Avatar :name is not valid. Valid mime types :mimes',
    'success' => 'Success',
    'error' => 'Error!',
    'file_upload' => 'File :name has not been uploaded because :error',
    'feedback_sent' => 'Thank you! Feedback has been sent.',
    'post_too_large' => 'Post size :size is exceeds the limit of :limit.'
];
