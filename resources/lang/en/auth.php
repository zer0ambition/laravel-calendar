<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'no_login' => 'No login',
    'logout' => 'Logged out',
    'success_login' => 'Successfull loggined',
    'unauth' => 'Unauthorized',
    'noauth' => 'Unauthorized',
    'success_registration' => 'Successfull registration. Verification message sent to the specified email address',
    'incorrect_creds' => 'Incorrect credentials given',
    'success_forgot' => 'Password reset link sent on your email',
    'invalid_reset_token' => 'Invalid reset token given',
    'success_reset' => 'Password has been successfuly changed',
    'no_permissions' => 'You do not have the necessary privileges',
    'no_permissions_change_role' => 'You do not have the necessary privileges to change user`s role',
];
